# Unity Audio Toolkit

A set of audio tools in C# for the Unity engine that allows sound designers to create audio events and customize their behaviours. 
The bulk of the audio logic code can be found in Assets/Scripts/AudioManager.cs
