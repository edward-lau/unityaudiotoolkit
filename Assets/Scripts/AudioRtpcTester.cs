﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRtpcTester : MonoBehaviour 
{
	public string paramName;
	public float paramValue = 50;

	void Update () 
	{
		AudioManager.instance.SetRtpc (paramName, paramValue);
	}
}
