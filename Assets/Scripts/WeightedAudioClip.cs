﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class WeightedAudioClip : ScriptableObject
{
	public AudioClip audioClip; 
	public int weight; 

	public WeightedAudioClip (AudioClip clip, int w)
	{
		audioClip = clip;
		weight = w;
	}
}