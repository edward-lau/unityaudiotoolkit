﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRtpc : ScriptableObject 
{
	public GameParameter gameParam;
	public rtpcScope scope;
	public rtpcParam eventParam;
	public AnimationCurve curve;
}
 
public enum rtpcScope
{
	global,
	gameObject  
}

public enum rtpcParam
{
	volume,
	pitch,
	lowPassCutoff,
	highPassCutoff
}