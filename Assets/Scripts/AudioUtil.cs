﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioUtil {

	private static float twelfthRootOfTwo = Mathf.Pow(2, 1.0f / 12);

	public static float DbToLinear (float dB)
	{
		float linear;

		if (dB > -46f)
		{
			linear = Mathf.Pow(10.0f, dB / 20.0f); // Convert dB value to linear
		}
		else
		{
			linear = 0f; // if randomVolume is -, linear volume is 0
		}

		return linear; // Return linear value
	}

	public static float LinearToDb (float linear)
	{
		float dB;

		if (linear != 0) 
		{	
			dB = 20.0f * Mathf.Log10 (linear);
		}
		else
		{
		dB = -46f;
		}

		return dB;
	}

	public static float StToLinear (float st)
	{
		float linear = (Mathf.Pow (twelfthRootOfTwo, st));

		return linear;
	}

	public static float LinearToSt (float linear)
	{
		float st = (Mathf.Log (linear, twelfthRootOfTwo));

		return st;
	}

}