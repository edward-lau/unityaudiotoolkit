﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (AudioLowPassFilter))]
[RequireComponent (typeof (AudioHighPassFilter))]
[RequireComponent (typeof (AudioSource))]
public class AudioEmitter : MonoBehaviour 
{
	public ActiveEvent activeEvent;
	public AudioSource source;
	public AudioLowPassFilter lpFilter;
	public AudioHighPassFilter hpFilter;
	public float playTime;
	public bool updateEmitter = false;
	private float currentUpdateTime = 0f;
	private float updateStep = 0.05f; // update every 100ms
	private RaycastHit occluderRayHit;
	private GameObject listenerObj;
	private GameObject attenuationObj;
	private float lerpTime = 0.1f;
	private float currentLerpTime;
	private Vector3 attenuatorPos;
	private Vector3 eventPos;

	void Awake ()
	{
		source = gameObject.GetComponent<AudioSource>();
		lpFilter = gameObject.GetComponent<AudioLowPassFilter>();
		hpFilter = gameObject.GetComponent<AudioHighPassFilter>();
		lpFilter.lowpassResonanceQ = 0.71f;
		hpFilter.highpassResonanceQ = 0.71f;
		source.playOnAwake = false;
		listenerObj = AudioManager.instance.listenerObj;
		if (AudioManager.instance.perspective == Perspective.ThirdPerson) 
		{
			attenuationObj = AudioManager.instance.attenuationObj;
		}
	}

	void Update ()
	{
		currentUpdateTime += Time.deltaTime;
		if (currentUpdateTime >= updateStep)
		{
			currentUpdateTime = 0f; // reset current update time
			if (!updateEmitter) 
			{
				updateEmitter = true;
				StartCoroutine (UpdateEmitter ());
			}

			if (listenerObj) 
			{
                if (!activeEvent.audioEvent.staticPriority)
                {
					activeEvent.priority = GetScaledPriority (activeEvent.audioEvent.maxDist, activeEvent.audioEvent.priority);
                }
                else
                {
                    activeEvent.priority = activeEvent.audioEvent.priority;
                }

				if (activeEvent.audioEvent.maxDist < Vector3.Distance (gameObject.transform.position, listenerObj.transform.position))
				{
					AudioManager.instance.VirtualizeEvent (activeEvent);
				}
			}
		}
	}

	private IEnumerator UpdateEmitter()
	{
		while (true)
		{
			if (AudioManager.instance.perspective == Perspective.ThirdPerson && activeEvent.audioEvent.ignoreThirdPersonMode == false && activeEvent.audioEvent.spatializeSound) 
			{
				SetThirdPersonPosition ();
			}

			activeEvent.targetVol = AddRtpc ((activeEvent.audioEvent.volume - activeEvent.baseEventVol) + activeEvent.fadeVol, RtpcProperty.volume);
			if (activeEvent.volume != activeEvent.targetVol) // prevents unecessary calculations
			{
				source.volume = AudioUtil.DbToLinear (activeEvent.targetVol);
				activeEvent.volume = activeEvent.targetVol;
			}

			activeEvent.targetPitch = AddRtpc (activeEvent.basePitch + (activeEvent.audioEvent.pitch - activeEvent.baseEventPitch), RtpcProperty.pitch);
			if (activeEvent.pitch != activeEvent.targetPitch) // prevents unecessary calculations
			{
				source.pitch = AudioUtil.StToLinear (activeEvent.targetPitch);
				activeEvent.pitch = activeEvent.targetPitch;
			}

			lpFilter.cutoffFrequency = AddRtpc (activeEvent.audioEvent.lpCutoff, RtpcProperty.lowPassCutoff);
			hpFilter.cutoffFrequency = AddRtpc (activeEvent.audioEvent.hpCutoff, RtpcProperty.highPassCutoff);
		
			yield return new WaitForSeconds (0f);
		}
	}

	private float AddRtpc (float value, RtpcProperty property)
	{
		if (activeEvent.audioEvent.rtpcByProp.ContainsKey (property))
		{
			float modSum = 0;

			for (int i = 0; i < activeEvent.audioEvent.rtpcByProp[property].Count; i++) 
			{
				RtpcSetting rtpc = activeEvent.audioEvent.rtpcByProp [property][i];
	
				float curveTime = rtpc.gameParam.globalValue / rtpc.gameParam.maxValue;
				modSum += rtpc.curve.Evaluate (curveTime);
			}
			return (value + modSum);
		}
		return value;
	}

	public void FadeIn (float targetVol, float fadeTime)
	{
		StartCoroutine (FadeInEmitter (targetVol, fadeTime));
	}

	public void FadeOut (float startVol, float fadeTime)
	{
		StartCoroutine (FadeOutEmitter (startVol, fadeTime));
	}

	private IEnumerator FadeInEmitter (float targetVol, float fadeTime)
	{
		float rate = 1.0f / fadeTime;
		float x = 0.0f;

		for (x = 0.0f; x <= 1.0f; x += Time.deltaTime * rate) 
		{
			activeEvent.fadeVol = Mathf.Lerp (-46f, targetVol, x);
			yield return null;
		}

		if (x >= 1.0f) 
		{
			activeEvent.fadeVol = targetVol;
		}
	}

	private IEnumerator FadeOutEmitter (float startVol, float fadeTime)
	{
		float rate = 1.0f / fadeTime;
		float x = 0.0f;

		for (x = 0.0f; x <= 1.0f; x += Time.deltaTime * rate) 
		{
			if (activeEvent.state == ActiveEventStates.Paused || activeEvent.state == ActiveEventStates.Virtual) break;
			activeEvent.fadeVol = Mathf.Lerp (startVol, -46f, x);
			yield return null; 
		}

		if (x >= 1.0f)
		{
			activeEvent.fadeVol = -46f;
		}
	}

	private int GetScaledPriority (float maxDist, int priority)
	{
		if (AudioManager.instance.perspective == Perspective.FirstPerson || activeEvent.audioEvent.ignoreThirdPersonMode) 
		{
			eventPos = this.gameObject.transform.position;
			attenuatorPos = listenerObj.transform.position;
		}
		else if (AudioManager.instance.perspective == Perspective.ThirdPerson && activeEvent.audioEvent.ignoreThirdPersonMode == false) 
		{
			eventPos = activeEvent.eventCaller.transform.position + activeEvent.localPosition;
			attenuatorPos = attenuationObj.transform.position;
		}

		float currentDistance = Vector3.Distance (eventPos, attenuatorPos); 
		int scaledPriority = (int)(priority * (1-(currentDistance / maxDist))); 

		if (scaledPriority > 0) 
		{
			return scaledPriority; 
		} 
		else
		{
			return 0;
		}								
	}

	private void SetThirdPersonPosition ()
	{
		Vector3 position = activeEvent.eventCaller.transform.position + activeEvent.localPosition;
		float distance = Vector3.Distance (position, attenuationObj.transform.position); 
		Vector3 normalizedVector = Vector3.Normalize (position - listenerObj.transform.position);
		this.transform.position = Vector3.Lerp (this.transform.position, listenerObj.transform.position + (distance * normalizedVector), Time.deltaTime * 10f); 
	}
}